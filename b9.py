import math
import matplotlib.pyplot as plt
import numpy
a=[float(i)/10 for i in range(1,51)]
print(a)
#####
m={}
for i in a:
    m[i]= float(2 * math.cos(i))
print(m)
#####
x= numpy.linspace(numpy.float(1/10),numpy.float(51/10),51)
f= numpy.cos(x)
plt.plot(2*x,f)
plt.show()